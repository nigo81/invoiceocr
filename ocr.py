#!/usr/bin python3
# -*- coding:UTF-8 -*-
# Author: nigo
from paddleocr import PaddleOCR
import re
from PIL import Image
import pandas as pd

def invoice_ocr(path):
    result = PaddleOCR().ocr(path)
    inform = []
    for line in result:
        inform.append(line[1][0])

    String2 = '【' + '】【'.join(inform) + '】'
    print(String2)
    if '发' in String2 or '票' in String2:
        print('yes')
    else:
        print('no')
        im = Image.open(path)
        out = im.transpose(Image.ROTATE_180)
        out.save(path)
        result = PaddleOCR().ocr(path)
        inform = []
        for line in result:
            inform.append(line[1][0])
        String2 = '【' + '】【'.join(inform) + '】'
    # 发票号码
    try:
        number = re.findall('(?<!\d)(\d{8})】',String2)[0]
    except:
        number=''
    invoice2 = re.sub('[a-zA-Z]','',String2)
    # 发票代码
    try:
        code = re.findall('(?<!\d)(\d{10,12})】',invoice2)[0]
    except:
        code = '' 
    # 校验码
    try:
        judge = re.findall('校验码[:：](\d+)】',invoice2)[0]
    except:
        judge = ''
    # 发票日期
    try:
        ymd = re.findall('\d{4}年\d{2}月\d{2}日',invoice2)[0]
    except:
        ymd = ''
    # 发票金额
    try:
        amounts = re.findall('￥\s*([0-9]*?\.\d*?)】',invoice2)
        amounts = [float(i) for i in amounts]
        amounts.sort()
        amount = amounts[-2]
        total_price = amounts[-1]
    except:
        amount = ''
        total_price = ''
    # 税率
    try:
        tax = re.findall('【(\d{1,2}%)】',invoice2)[0]
    except:
        tax = ''
    # 购买方
    try:
        purch_name = re.findall('称[:：](.*?)】',invoice2)[0]
    except:
        purch_name = ''
    # 销售方
    try:
        sale_name = re.findall('称[:：](.*?)】',invoice2)[1]
    except:
        sale_name = ''
    try:
        purch_id = re.findall('纳税人识别号[:：](\d+)】',invoice2)[0]
        sale_id = re.findall('纳税人识别号[:：](\d+)】',invoice2)[1]
    except:
        purch_id = ''
        sale_id = ''
    info = [[path,purch_name,purch_id,sale_name,sale_id,code,number,judge,ymd,total_price,amount,tax]]
    df = pd.DataFrame(info,columns=['路径','购买方','购买方纳税人识别号','销售方','销售人纳税人识别号','发票代码','发票号','校验码','日期','总金额','销售金额','税率'])
    return df
        

if __name__ == "__main__":
    path = './img/test.png'
    result = invoice_ocr(path)
    print(result)
